package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {
        try {
            File file = new File(parkDatafilePath);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            ArrayList<String> stringArrayList = new ArrayList<>();
            HashMap<String, String> nameAndCost = new HashMap<>();
            ArrayList<ParkParsingException.ParkValidationError> errors = new ArrayList<>();
            while (true) {
                String string = bufferedReader.readLine();
                if (string == null) {
                    break;
                }
                stringArrayList.add(string);
                String[] fieldsString = string.split(":");
                if (fieldsString.length == 2) {
                    String name = fieldsString[0].replace("\"", "");
                    String notValidValue = fieldsString[1].replace("\"", "");
                    String value = notValidValue.replace(" ", "");
                    nameAndCost.put(name, value);
                }
            }
            bufferedReader.close();
            Class parkClass = Park.class;
            Constructor<? extends Park> constructor = parkClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            Park park = constructor.newInstance();
            Field[] fields = parkClass.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                String[] fieldName = fields[i].toString().split("\\.");
                String stringValueOfField = fieldName[fieldName.length - 1];

                Field declaredField = parkClass.getDeclaredField(stringValueOfField);
                declaredField.setAccessible(true);


                if (stringValueOfField.equals("foundationYear")) {
                    if(!(nameAndCost.get("foundationYear").equals(""))  ) {
                        String[] time = nameAndCost.get("foundationYear").split("-");
                        declaredField.set(park, LocalDate.of(Integer.parseInt(time[0]), Integer.parseInt(time[1]), Integer.parseInt(time[2])));
                    }else {
                        ParkParsingException.ParkValidationError error = new ParkParsingException.ParkValidationError(stringValueOfField, "поле пустое " + stringValueOfField);
                        errors.add(error);
                    }
                } else {
                    declaredField.set(park, nameAndCost.get(stringValueOfField));
                }


                for (Annotation annotation : declaredField.getAnnotations()) {
                    if (annotation instanceof FieldName) {
                        String annotationParametr = ((FieldName) annotation).value();
                        declaredField.set(park, nameAndCost.get(annotationParametr));
                    }
                    if (annotation instanceof MaxLength) {
                        if ((nameAndCost.get(stringValueOfField).length()) > ((MaxLength) annotation).value()) {
                            ParkParsingException.ParkValidationError error = new ParkParsingException.ParkValidationError(stringValueOfField, "превышение символов в " + stringValueOfField);
                            errors.add(error);
                        }
                        if(annotation instanceof NotBlank && nameAndCost.get(stringValueOfField)==""){
                            ParkParsingException.ParkValidationError error = new ParkParsingException.ParkValidationError(stringValueOfField, "значенияе поле пустое  " + stringValueOfField);
                            errors.add(error);
                        }
                    }
                }
            }
            if(errors.size()==0){
                System.out.println("При парсинге файла не возникло ошибок ");
                return park;
            }else {
                throw new ParkParsingException(" При парсинге файла возникли следующие ошибки ",errors);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
